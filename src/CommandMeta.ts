export type CommandPermissionLevel = "Bot Owner"
  | "Bot Administrator"
  | "Bot Supporter"
  | "Server Owner"
  | "Bot Commander"
  | "Administrator"
  | "Moderator"
  | "User";

type CommandUsageType = "string"
  | "number"
  | "boolean"
  | "channel"
  | "role"
  | "user"
  | "member";

export type CommandUsage = {
  argType: CommandUsageType,
  argName: string 
};

export type CommandConf = {
  enabled: boolean,
  permLevel: CommandPermissionLevel,
  guildOnly: boolean,
  aliases: string[],

  hidden?: boolean,
  badge?: string,
  settings?: string[]
};

export type CommandHelp = {
  description: string,
  usage: CommandUsage[],
  category?: string
};

// // quick test here
// const help: CommandHelp = {
//   description: "b",
//   usage: [{
//     argType: "string",
//     argName: "argument"
//   }],
//   category: "b"
// };