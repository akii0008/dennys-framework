import { Client, ClientEvents, ClientOptions, Snowflake } from "discord.js";
import { Routes } from "discord-api-types/v9";
import { readdirSync, PathLike } from "node:fs";
import { join } from "path";

import InteractionCommand from "./InteractionCommand";
import Event from "./Event";
import Logger from "./util/logger.js";
import MessageCommand from "./MessageCommand";
import { REST } from "@discordjs/rest";

export interface DennysOptions {
  discordOptions: ClientOptions,
  prefix: string,
  commandsFolder: PathLike,
  events: Array<keyof ClientEvents>,
  logToFile?: boolean,

  token: string,
  clientID: Snowflake,
  guildID: Snowflake
}

export default class Bot extends Client {

  private readonly commandsFolder: PathLike;

  public slashCommands: Map<string, InteractionCommand> = new Map();
  public messageCommands: Map<string, MessageCommand> = new Map();
  public readonly events = new Map();

  public readonly logger = new Logger();

  constructor(options: DennysOptions) {
    super(options.discordOptions);

    this.logger.log("Starting bot...");

    this.commandsFolder = options.commandsFolder;

    const commandFiles = readdirSync(this.commandsFolder);
    commandFiles.forEach(async commandFile => {
      if (!commandFile.endsWith(".ts") && !commandFile.endsWith(".js")) {
        return this.logger.warn(`Command ${commandFile} does not end in .ts/.js`);
      }

      const command = await import(join(this.commandsFolder as string, commandFile));
      const CommandClass = new command.default();

      if (CommandClass instanceof MessageCommand) {

        this.logger.verbose(`Loading MessageCommand ${commandFile}...`);
        this.messageCommands.set(CommandClass.name, CommandClass);

      } else if (CommandClass instanceof InteractionCommand) {

        this.logger.verbose(`Loading InteractionCommand ${commandFile}...`);
        this.slashCommands.set(CommandClass.name, CommandClass);

      } else {
        throw new TypeError(`${commandFile}: Invalid command type. Must extend one of \`InteractionCommand\` or \`MessageCommand\``);
      }

    });

    options.events.forEach((eventName: keyof ClientEvents) => {
      const event = new Event();
      this.events.set(eventName, event);
      this.on(eventName, event.run.bind(event, this));
      this.logger.verbose(`Listening to event ${eventName}`);
    });

    super.login(options.token);
    this.once("ready", () => {

      // This functionality is in the ready event because it gets fired only after all the commands are loaded.. i believe.
      const rest = new REST({ version: "9" }).setToken(options.token);
      (async () => {
        try {
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          const slashCommandsInJSON = Array.from(this.slashCommands).map(([_, CommandClass]) => CommandClass.builder.toJSON());

          await rest.put(
            Routes.applicationGuildCommands(options.clientID, options.guildID),

            { body: slashCommandsInJSON } // Hack to convert a Map to an Array that I can iterate over in a one-liner
          );
        } catch (error) {
          if (error instanceof Error) {
            this.logger.error(error);
          }
        }

      })();

      this.logger.log(`Client ready. Logged in as ${this.user?.tag} (${this.user?.id})`);
    });

    if (this.slashCommands.size !== 0 && !this.events.has("interactionCreate")) {
      this.logger.warn("Slash commands were registered but you are not listening to the `interactionCreate` event.");
    }

    if (this.messageCommands.size !== 0 && !this.events.has("messageCreate")) {
      this.logger.warn("Message events were registered but you are not listening to the `messageCreate` event.");
    }

  }

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public override login() {
    throw new Error("This method is automatically called when creating the bot. No need to call it again.");
  }

}
