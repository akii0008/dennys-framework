/* eslint-disable @typescript-eslint/no-unused-vars */
// harmless disable

import { ClientEvents } from "discord.js";
import Bot from "./Client";

type MiddlewareData<T extends keyof ClientEvents> = ClientEvents[T] | [...ClientEvents[T], ...unknown[]];
type MiddlewareFn<T extends keyof ClientEvents> = (client: Bot, ...params: MiddlewareData<T>) => MiddlewareData<T> | null;

export default class Event<T extends keyof ClientEvents> {
  
  protected middleware: MiddlewareFn<T>[] = [];

  // ClientEvents[T] is an array, so it fits with the ...params.
  public run(client: Bot, ...params: MiddlewareData<T>): void {
    let temporaryParameters = params;
    
    for (const middlefn of this.middleware) {
      const result = middlefn(client, ...temporaryParameters);
      // we've reached the end of the chain OR we're short circuiting 
      if (result === null) {
        break;
      }

      temporaryParameters = result;
    }
  }

  public use(fn: MiddlewareFn<T>) {
    this.middleware.push(fn);
  }
}

/* 
const MessageCreateEvent = new Event<"messageCreate">();
MessageCreateEvent.use((client, message, next) => {
  next(message.content = message.content.replaceAll)
})

*/

// function test(...params: string[]) {
//   return;
// }

// test("a", "b", "c");
// test(["a", "b", "c"]);