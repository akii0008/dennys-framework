// Logger class for easy and aesthetically pleasing console logging

import chalk from "chalk";
import moment from "moment"; // TODO: switch away from moment
import { inspect } from "util";

export default class Logger {

  private readonly timestamp: string = chalk.grey(`[${moment().format("MM/DD/YYYY HH:mm:ss")}]`);

  constructor(customTimestamp?: string) {
    if (customTimestamp)
      this.timestamp = customTimestamp;
  }

  public log(info: string): void {
    return console.log(`${this.timestamp} ${chalk.blue("info:")} ${info} `);
  }

  public warn(info: string): void {
    return console.warn(`${this.timestamp} ${chalk.yellow("warn:")} ${info} `);
  }

  public error(info: Error | string): void {
    if (info instanceof Error) {
      info = (info.stack ?? info.message)
        .split("\n")
        .join(`\n${this.timestamp} ${chalk.red("error: ")}`);

      console.error(`${this.timestamp} ${chalk.red("error:")} ${info}`);
    } else {
      console.error(`${this.timestamp} ${chalk.red("error:")} ${info}`);
    }
  }

  public fatal(info: Error | string): void {
    if (info instanceof Error) {
      info = (info.stack ?? info.message)
        .split("\n")
        .join(`\n${this.timestamp} ${chalk.bgRed.white("FATAL: ")}`);

      console.error(`${this.timestamp} ${chalk.bgRed.white("FATAL:")} ${chalk.red(info)}`);
    } else {
      console.error(`${this.timestamp} ${chalk.bgRed.white("FATAL:")} ${chalk.red(info)}`);
    }
  }

  public verbose(info: unknown): void {
    if (typeof info === "object")
      info = inspect(info, { depth: 0, colors: true });

    if (info instanceof Error)
      info = (info.stack ?? info.message)
        .split("\n")
        .join(`\n${this.timestamp} ${chalk.grey("verbose ")}`);

    return console.log(`${this.timestamp} ${chalk.gray("verbose:")} ${chalk.gray(info)} `);
  }

}
