import { CommandConf, CommandHelp } from "./CommandMeta";
import Bot from "./Client";
import { Message } from "discord.js";

export default abstract class MessageCommand {
  public abstract readonly name: string;
  public abstract readonly config: CommandConf;
  public abstract readonly help: CommandHelp;

  public abstract run(client: Bot, message: Message, args: string[]): void;
}