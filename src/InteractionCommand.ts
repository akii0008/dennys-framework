import Bot from "./Client";
import { CommandInteraction } from "discord.js";
import { SlashCommandBuilder } from "@discordjs/builders";

export default abstract class InteractionCommand {
  public abstract readonly name: string;
  public abstract readonly builder: SlashCommandBuilder;

  public abstract run(client: Bot, interaction: CommandInteraction): void;
}